import sqlite3
from flask import Flask, jsonify, request
DATABASE_NAME = "residencia.db"

def get_db():
	conn = sqlite3.connect(DATABASE_NAME)
	return conn

def create_table():
	tables = [
		"""CREATE TABLE IF NOT EXISTS residencia(
				id INTEGER PRIMARY KEY AUTOINCREMENT,
				name TEXT NOT NULL)"""]
	db = get_db()
	cursor = db.cursor()
	for table in tables:
		cursor.execute(table)

app = Flask(__name__)

@app.route("/peticion", methods=["GET"])
def get_mensaje():
	db = get_db()
	cursor = db.cursor()
	query = "SELECT id, name FROM residencia"
	cursor.execute(query)
	return print(cursor.fetchall())


@app.route("/peticion", methods=["POST"])
def insert_peticion():
	mensaje_peticion = request.get_json()
	mensaje = mensaje_peticion["name"]
	db = get_db()
	cursor = db.cursor()
	statement = "INSERT INTO residencia(name) VALUES (?)"
	cursor.execute(statement, [mensaje])
	db.commit()
	return "Almacenado en BBDD"

@app.route("/peticion/<id>", methods=["DELETE"])
def delete_peticion(id):
	db = get_db()
	cursor = db.cursor()
	statement = "DELETE FROM residencia WHERE id = ?"
	cursor.execute(statement, [id])
	db.commit()
	return "Eliminado " + id + " de BBDD"


if __name__ == "__main__":
	create_table()
	app.run(host='0.0.0.0', port=3000, debug=False)
