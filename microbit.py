from microbit import *

def main():
    uart.init(baudrate=9600)
    while True:
        sleep(500)
        msg = uart.read()
        if (msg is not None):
            display.show(str(msg, 'UTF-8'))
            msg = "Recibido: " + str(msg)[2:-1]
            print(str(msg, 'UTF-8'))

        else:
            display.clear()
if __name__ == "__main__":
    main()
